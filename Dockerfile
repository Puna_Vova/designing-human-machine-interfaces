FROM nginx

COPY index.html /usr/share/nginx/html/
COPY index.js /usr/share/nginx/html/
COPY index.css /usr/share/nginx/html/
#COPY test.php /usr/share/nginx/html/

# Конфигурируем Nginx для обработки PHP
COPY default.conf /etc/nginx/conf.d/default.conf

# Открываем порт 80 для входящих соединений
EXPOSE 80

# Запускаем PHP-FPM и Nginx в фоновом режиме при запуске контейнера
# CMD service php7.4-fpm start && nginx -g "daemon off;"