function processForm(event) {
    event.preventDefault();

    var nameInput = document.getElementById("name");
    var name = nameInput.value;

    var greeting = document.createElement("h2");
    greeting.textContent = "Hello, " + name + "!";
    document.body.appendChild(greeting);
}